import implementation.QuestionAndAnswer;
import org.junit.Test;
import utils.SecondaryService;

import java.io.*;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class QuestionAnswerTest {

    private QuestionAndAnswer questionAnswer = new QuestionAndAnswer();
    private SecondaryService secondaryService = new SecondaryService();

    //What is Peters favorite food? "Pizza""Spaghetti""Ice cream"
    String expectedAnswerForQuestion = "Who is Messi?\"footballer\"\"A Father\"\"The best footballer";
    String question = "Who is Messi?";

    /*here we want to test if the question answer returns a list of two parts: [String question, String answers]*/
    String testFile="test.txt";
    @Test
    public void ifTheQuestionFormatIsRespectedTest() {

        int actualTokens = questionAnswer.sizeOfTokenizeInput(expectedAnswerForQuestion);

        assertEquals(actualTokens , 2 );
    }

   @Test
    public void addQuestionFirstTest() throws IOException {
        String str = "Who is Messi?footballer A Father The best footballer";
        Map<String , List<String>> actualTokens = secondaryService.addQuestionAndAnswerInFile(expectedAnswerForQuestion);

        String actualQuestionAnswer = actualTokens.toString().replace("}","").replace("{", "")
                .replace("=","").replace(",","").replace("[","").replace("]", "").trim();
        assertEquals(str, actualQuestionAnswer);

    }

    @Test
    public void addQuestionSecondTest() throws IOException {
        String str = "Who is Messi?footballer A Father The best footballer";
        Map<String , List<String>> actualTokens = secondaryService.addQuestionAndAnswerInFile(expectedAnswerForQuestion);
        String actual = actualTokens.get("Who is Messi?").toString()
                .replace("]","").replace("[", "").trim();
        assertEquals("footballer, A Father, The best footballer" , actual);

    }

    @Test
    public void askQuestionTest() throws IOException {
        String str = "Who is Messi?footballer A Father The best footballer";
        Map<String , List<String>> actualTokens = questionAnswer.askquestionFile(question);
        String actual = actualTokens.get("Who is Messi?").toString()
                .replace("]","").replace("[", "").trim();
        assertEquals("the answer to life, universe and everything is 42" , actual);

    }



}
