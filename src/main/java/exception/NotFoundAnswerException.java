package exception;

/*
 This exception is been throw when the question asked has no answer register in the system
 */
public class NotFoundAnswerException extends RuntimeException{


    public NotFoundAnswerException(String message) {
        super(message);
    }
    public NotFoundAnswerException(String message, Throwable cause) {
        super(message, cause);
    }
}
