package exception;

public class MaximumStringLengthException extends  RuntimeException{

    public MaximumStringLengthException(String message) {
        super(message);
    }
    public MaximumStringLengthException(String message, Throwable cause) {
        super(message, cause);
    }
}
