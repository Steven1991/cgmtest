package exception;

public class QuestionAnswerAlreadyExistException extends  RuntimeException{

    public QuestionAnswerAlreadyExistException(String message) {
        super(message);
    }

}
