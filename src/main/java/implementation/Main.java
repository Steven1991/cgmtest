package implementation;
import exception.MaximumStringLengthException;
import exception.NotFoundAnswerException;
import exception.QuestionAnswerAlreadyExistException;
import exception.QuestionNotConformException;
import utils.SecondaryService;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main (String[] argv) throws IOException {
        //instance of the Class that is like a facade to our project
        QuestionAndAnswer questionAnswers = new QuestionAndAnswer();

        //instance of the class containing usefull methodes
        SecondaryService secondaryService = new SecondaryService();

        boolean input = false;
        //horizontal space, i use it to parse the outpout from my file
        String hspace= "\t";

        // vertical space, i use it to parse the outpout from my file
        String newLine = System.getProperty("line.separator");
        Scanner scan = new  Scanner(System.in);

        String answerWillBe ="* Answer will be:";
        do{
            System.out.println("type 1 if you want to ask a question or 2 if you want to add question and 3 to exit");
            int compter = scan.nextInt();
            switch (compter){
                case 1 :
                    scan.nextLine();
                    System.out.println("ask your question");
                    String askQuestion = scan.nextLine();
                    try{
                    int testQuestionExistanceInFile = secondaryService.isQuestionExistInFile(askQuestion);
                    switch(testQuestionExistanceInFile)
                    {

                            // if no answer existe for the current question
                            case 0:
                                System.out.println(hspace+"* Answer will be : ");
                                System.out.println(hspace+hspace+"* the answer to life, universe and everything is 42");
                                break;

                            case 1:
                                secondaryService.fetchAnswerInFile(askQuestion);
                                questionAnswers.askquestionFile(askQuestion).forEach((k,v)->System.out.println(hspace+answerWillBe+v.toString().
                                        replace("[","").replace(" ]","").replace(",]","")
                                        .replace(",]","")
                                        .replace(",",newLine+hspace+hspace+" * ")));
                                break;

                        default:
                            System.exit(0);
                        }



                    }catch (QuestionNotConformException e){
                        e.printStackTrace();
                    }
                    catch (NotFoundAnswerException ex){
                        ex.printStackTrace();
                    }
                    catch (MaximumStringLengthException em){
                        em.printStackTrace();
                    }
                    break;
                case 2 :
                    scan.nextLine();
                    System.out.println("add a question");
                    String addQuestion = scan.nextLine();
                    try {
                    int testQuestionAndAnswerExistanceInFile = secondaryService.isQuestionAnswerExistInFile(addQuestion);
                    switch(testQuestionAndAnswerExistanceInFile) {

                           case 0: questionAnswers.addQuestionAndAnswerInFile(addQuestion);
                                   System.out.println("successfully Added");

                                 break;
                        case 1:
                            throw  new QuestionAnswerAlreadyExistException("This question already exist in the file");
                           //
                        }
                    }   catch (QuestionNotConformException e) {
                            e.printStackTrace();
                        } catch (NotFoundAnswerException ex) {
                            ex.printStackTrace();
                        } catch (MaximumStringLengthException em) {
                            em.printStackTrace();
                        }
                         catch (QuestionAnswerAlreadyExistException eq) {
                        eq.printStackTrace();
                    }
                    break;
                case 3:
                    input = true;
                    break;
                default:
                    input = false;
                    break;

            }
        } while(!input);
        //What is Peters favorite food? "Pizza""Spaghetti""Ice cream"
        //What is Steves favorite food? "Pizza""Spaghetti""Eru"
        //Who is Steve? "Coder""Programmer""Father"
    }
}
