package implementation;

import exception.MaximumStringLengthException;
import exception.QuestionNotConformException;
import utils.SecondaryService;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/*
    To realize this small game, i used Files and Collections: Questions and Answers, are been stored in a file,
    and whenever a question is been ansked, we go and search in the file, if the question existe we return results.
    We also have a secon file to insert set of questions that have been asked,and the time at which it was asked.
     that second file can help us to do some statistics.

 */
public class QuestionAndAnswer {

    //this map contents the set of my questions an their corresponding answer(s)
    public static HashMap<String, List<String>> questionAnwersRepository = new HashMap<>();

    //this map will reply to the user who is asking questions
    public static HashMap<String, List<String>> answerToQuestion = new HashMap<>();

    private SecondaryService secondaryService = new SecondaryService();

    String answerQuestionFilePath="questionAnswer.txt";

    String questionHistorikFilePath="questionHistoric.txt";


    /*this method takes in a String of questions and answers, and returns a list with a size of 2
     ths size of two is beacause after beeing tokenized, the String will become a lsit with the form List [String Question, String answers]
     this method is suitable for my tests
    */

    public int sizeOfTokenizeInput(String str) {
        List<String> tokens = new ArrayList<>();
        if (!str.contains("?")){
            throw  new QuestionNotConformException("This question is not conform !Make sure that your question ends with a question mark.");
        }
        StringTokenizer tokenizer = new StringTokenizer(str, "?");
        while (tokenizer.hasMoreElements()) {
            tokens.add(tokenizer.nextToken());
        }
        return tokens.size();
    }

    /*this methode tasks as input a question (a string) example: What is Peters favorite food? "Pizza""Spaghetti""Ice cream"
    register the information in a file called questionAnswer.txt, and returns a Map of <qestion , and List of answers>
    */
    public Map<String, List<String>> addQuestionAndAnswerInFile(String str) throws IOException {

        QuestionAndAnswer questionAnswer = new QuestionAndAnswer();
        String oldContent = "";
        BufferedWriter writer  = new BufferedWriter( new FileWriter( answerQuestionFilePath, true));

        if(questionAnswer.sizeOfTokenizeInput(str)!=2){
            throw  new QuestionNotConformException("Make sure that your question ends with a question mark");
        }
        if (!str.contains("?")){
            throw  new QuestionNotConformException("Make sure that your question ends with a question mark");
        }
        List<String>  tokens = Collections.list(new StringTokenizer(str, "?")).stream().map(token -> (String) token).collect(Collectors.toList());
        String question = tokens.get(0)+"?";
        String answer = tokens.get(1);
        List<String> answerss = Collections.list(new StringTokenizer(answer, "\"\"")).stream().map(token -> (String) token).collect(Collectors.toList());

        if(question.length()>256 || answer.length()>256){
            throw  new MaximumStringLengthException("Answer or question with more than 255 char is not possible");
        }

       if(secondaryService.isQuestionAnswerExistInFile(str)==0 && secondaryService.isQuestionAnswerExistInFile(question)==0){
            writer.write(str);
            writer.newLine();
            if ( writer != null)
                writer.close( );
           questionAnwersRepository.put(question, answerss);
        }
       else if(secondaryService.isQuestionExistInFile(str)==1 || secondaryService.isQuestionExistInFile(question)==1){
            BufferedReader reader = new BufferedReader(new FileReader(answerQuestionFilePath));
           System.out.println("Bonjour!!!12");

            String oldString = Files.lines(Paths.get(answerQuestionFilePath)).filter(lines -> lines.contains(question)).collect(Collectors.toList()).toString().replace("[", "").replace("]", "");

            //Replacing oldString with newString in the oldContent

            String newContent = oldContent.replaceAll(oldString, str);

            //Rewriting the input text file with newContent

           FileWriter fileWriter = new FileWriter(answerQuestionFilePath, true);

           fileWriter.write(newContent);

            try
            {
                //Closing the resources

                reader.close();

                fileWriter.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
       return questionAnwersRepository;
    }


    /*
  this method takes the question asked by a user and return results (either the question follewed by the
  corresponing answers, or either the following message: "the answer to life, universe and everything is 42"
   */
    public  Map<String, List<String>> askquestionFile(String question) throws  IOException{

        List<String> response = new ArrayList<String>();
        QuestionAndAnswer questionAnswer = new QuestionAndAnswer();
        BufferedWriter writer = new BufferedWriter( new FileWriter(questionHistorikFilePath, true));
        if(!(new File(answerQuestionFilePath).exists())){
             File file = new File(answerQuestionFilePath);
              file.createNewFile();
        }
        if (!question.contains("?") || !secondaryService.isOnlyQuestion(question)){
            throw  new QuestionNotConformException("Make sure that your question ends with a question mark");
        }

        if(question.length()>256){
            throw  new MaximumStringLengthException("Answer or question with more than 255 char is not possible");
        }
        if(secondaryService.isQuestionExistInFile(question)==1){
            answerToQuestion.clear();
            response  =  secondaryService.fetchAnswerInFile(question);
            answerToQuestion.put(question, response);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
            String moment = sdf.format(new Date());
            String toFile = question+ "  : asked at " +moment+ " status : "+ " answered";
            writer.write(toFile);
            writer.newLine();
            if (writer != null)
                writer.close();
           } else {
            answerToQuestion.clear();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
            String moment = sdf.format(new Date());
            String error = "the answer to life, universe and everything is 42";
            String toFile = question+" : asked at "+moment+" status :"+ "No answer";
            writer.write(toFile);
            writer.newLine();

            if (writer != null)
                writer.close();
            response.add(error);
            answerToQuestion.put(question, response);

        }

         return  answerToQuestion;
    }


}
