package utils;

import exception.MaximumStringLengthException;
import exception.NotFoundAnswerException;
import exception.QuestionAnswerAlreadyExistException;
import exception.QuestionNotConformException;
import implementation.QuestionAndAnswer;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SecondaryService {

    public static HashMap<String, List<String>> questionAnwersRepository = new HashMap<>();
    //this file contains the different test samples
    String testFile="testFile.txt";
    //this map will reply to the user who is asking questions
    public static HashMap<String, List<String>> answerToQuestion = new HashMap<>();
    String answerQuestionFilePath="questionAnswer.txt";
    String questionHistorikFilePath="questionHistoric.txt";
    public Map<String, List<String>> addQuestionAndAnswerInFile(String str) throws IOException {


        BufferedWriter writer  = new BufferedWriter( new FileWriter(testFile, true));


        if (!str.contains("?")){
            throw  new QuestionNotConformException("Make sure that your question ends with a question mark");
        }
        List<String>  tokens = Collections.list(new StringTokenizer(str, "?")).stream().map(token -> (String) token).collect(Collectors.toList());
        String question = tokens.get(0)+"?";
        String answer = tokens.get(1);
        List<String> answerss = Collections.list(new StringTokenizer(answer, "\"\"")).stream().map(token -> (String) token).collect(Collectors.toList());

        if(question.length()>256 || answer.length()>256){
            throw  new MaximumStringLengthException("Answer or question with more than 255 char is not possible");
        }

            /*SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
            String moment = sdf.format(new Date());
            String toFile = question+answer+" : tested at "+moment;*/
            String toFile = answer;
            writer.write(toFile);
            writer.newLine();
            if (writer != null)
                writer.close();
                questionAnwersRepository.put(question, answerss);

          return questionAnwersRepository;
    }


     /*isOnlyQuestion Verifies if the input is realy a question or is different from that. exemple if than to ask a question,
    you write :Who is Steve? "Coder""Programmer""Father", it needs to ask you to insert only a question
    */

    public Boolean isOnlyQuestion(String str){
        Boolean result=false;
        List<String>  tokens = Collections.list(new StringTokenizer(str, "?")).stream().map(token -> (String) token).collect(Collectors.toList());
        //String question = tokens.get(0);
        //it means is a list of size 2: first index keeps the question and the second keeps answers
        if(tokens.size()==2){
            result=false;
        } else{
            result =true;
        }
        return result;
    }


    /*
       this methode searchs if a question exists in questionAnswer.txt file,
       and if it founds it, it returns 1 if not it returns 0
      */
    public int isQuestionExistInFile(String questionAnswer) throws  IOException{

        int result = 0;
        SecondaryService secondaryService = new SecondaryService();
        if(!secondaryService.isOnlyQuestion(questionAnswer)){
            throw  new QuestionNotConformException("Make sure that your question ends with a question mark");
        }
        if(!(new File(answerQuestionFilePath).exists())){
            new File(answerQuestionFilePath).createNewFile();
            result = 0;
            //throw new NotFoundAnswerException("this question has no answer yet !!!");
        } else if(new File(answerQuestionFilePath).exists()) {
            File file = new File(answerQuestionFilePath);
            file.exists();
            Stream<String> stream = Files.lines(Paths.get(answerQuestionFilePath));
            int tester= (int)stream.filter(lines -> lines.contains(questionAnswer)).count();
            //System.out.println("2");
            if(tester>0){
                result= 1;
            }
            else {
                result= 0;
                //throw new NotFoundAnswerException("This question has no answer yet !!!");
            }
        }
        return  result;
    }

    public int isQuestionExistInFileForTest (String questionAnswer) throws  IOException{

        int result = 0;
        SecondaryService secondaryService = new SecondaryService();
        if(!secondaryService.isOnlyQuestion(questionAnswer)){
            throw  new QuestionNotConformException("Make sure that your question ends with a question mark");
        }
        if(!(new File(answerQuestionFilePath).exists())){
            new File(answerQuestionFilePath).createNewFile();
            result = 0;
            throw new NotFoundAnswerException("this question has no answer yet !!!");
        } else if(new File(answerQuestionFilePath).exists()) {
            File file = new File(answerQuestionFilePath);
            file.exists();
            Stream<String> stream = Files.lines(Paths.get(answerQuestionFilePath));
            int tester= (int)stream.filter(lines -> lines.contains(questionAnswer)).count();
            //System.out.println("2");
            if(tester>0){
                result= 1;
            }
            else {
                result= 0;
                throw new NotFoundAnswerException("This question has no answer yet !!!");
            }
        }
        return  result;
    }


    /*
     this method takes in a question, searchs the answaer in our questionAnswer.txt file and returns results in a List
     */
    public List<String> fetchAnswerInFile(String question) throws  IOException{

        List<String> tester = new ArrayList<>();
        List<String> result = new ArrayList<>();
        Stream<String> stream = Files.lines(Paths.get(answerQuestionFilePath));

        String testers= stream.filter(lines -> lines.contains(question)).collect(Collectors.toList()).toString();

        List<String> answerList1 = Collections.list(new StringTokenizer(testers, "?")).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
        List<String> answerList = Collections.list(new StringTokenizer(answerList1.get(1), "\"")).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());

        if(!tester.isEmpty()){
            answerList.remove(0) ;
            result = answerList;
        }
        else {
            result = null;
        }

        return  answerList ;
    }


    public int isQuestionAnswerExistInFile(String question) throws  IOException{

        int result = 0;
        QuestionAndAnswer qa = new QuestionAndAnswer();
        if(!(new File(answerQuestionFilePath).exists())){
            File file = new File(answerQuestionFilePath);
            file.createNewFile();
            result = 0;
        } else if(new File(answerQuestionFilePath).exists()) {
            new File(answerQuestionFilePath).exists();
            Stream<String> stream = Files.lines(Paths.get(answerQuestionFilePath));
            int tester= (int)stream.filter(lines -> lines.contains(question)).count();
            if(tester>0){
                result= 1;

            }
            else {
                result= 0;
            }
        }
        return  result;
    }

     /*this methode takes as input a question (a string) and it's answers exemple : What is Peters favorite food? "Pizza""Spaghetti""Ice cream"
      it returns a list of String on the form off [question?, answaers];
     */

    public List<String> addQuestionAndAnswer(String str) {

        List<String> answerList = Collections.list(new StringTokenizer(str, "?")).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());

        if(answerList.get(0).length()>256 || answerList.get(1).length()>256){
            throw  new MaximumStringLengthException("Answer or question with more than 255 char is not possible");
        }
        if (!answerList.get(0).contains("?")){
            throw  new QuestionNotConformException("Make sure that your question ends with a question mark");
        }

        return answerList;
    }

    public  Map<String, List<String>> askquestionFile(String question) throws FileNotFoundException, IOException{

        List<String> response = new ArrayList<String>();
        SecondaryService questionAnswer = new  SecondaryService();
        BufferedWriter writer = new BufferedWriter( new FileWriter(questionHistorikFilePath, true));

        if (!question.contains("?") || !questionAnswer.isOnlyQuestion(question)){
            throw  new QuestionNotConformException("Make sure that your question ends with a question mark");
        }
        if(question.length()>256){
            throw  new MaximumStringLengthException("Answer or question with more than 255 char is not possible");
        }
        if(questionAnswer.isQuestionExistInFile(question)==1){
            answerToQuestion.clear();
            response  =  questionAnswer.fetchAnswerInFile(question);
            answerToQuestion.put(question, response);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
            String moment = sdf.format(new Date());
            String toFile = question+" : asked at "+moment+"status :"+ "answered";
            writer.write(toFile);
            if (writer != null)
                writer.close();
        } else {
            answerToQuestion.clear();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
            String moment = sdf.format(new Date());
            String error = "the answer to life, universe and everything is 42";
            String toFile = question+" : asked at "+moment+" status :"+ "No answer";
            writer.write(toFile);

            if (writer != null)
                writer.close();
            response.add(error);
            answerToQuestion.put(question, response);

        }

        return  answerToQuestion;
    }

    public List<String> fetchAnswerInFileForTest(String question) throws  IOException{

        List<String> tester = new ArrayList<>();
        List<String> result = new ArrayList<>();
        Stream<String> stream = Files.lines(Paths.get(testFile));
        List<String> answerList = new ArrayList<>();
        String testers= stream.filter(lines -> lines.contains(question)).collect(Collectors.toList()).toString();

       /* List<String> answerList = Collections.list(new StringTokenizer(testers, "?")).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
       /* List<String> answerList = Collections.list(new StringTokenizer(answerList1.get(1), "\"")).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());*/

        if(!tester.isEmpty()){
           // answerList.remove(0) ;
            answerList.add(testers);
            result = answerList;
        }
        else {
            result = null;
        }

        return  answerList ;
    }
}
